import { Injectable } from '@nestjs/common';
import { MailDto } from './dto/mail.dto';
import {Client, ClientProxy, Transport} from '@nestjs/microservices';
import {ConfigService} from '../config/config.service';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {
    constructor(private readonly configService: ConfigService) {}

    @Client({ transport: Transport.NATS })
    client: ClientProxy;

    async sendMail(mailDto: MailDto): Promise<Observable<any>> {
       return await this.client.send({cmd: this.configService.get('SEND_COMMAND')}, mailDto);
     }
}
