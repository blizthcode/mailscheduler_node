import { IsEmail, IsNotEmpty, IsPositive, MaxLength, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class MailDto {
  @ApiModelProperty({example: 'test@example.com'})
  @IsEmail()
  @IsNotEmpty()
  From: string;

  @ApiModelProperty({example: 'test@example.com'})
  @IsEmail()
  @IsNotEmpty()
  To: string;

  @ApiModelProperty({example: 'Test subject'})
  @IsNotEmpty()
  Subject: string;

  @ApiModelProperty({example: 'NewUser'})
  @IsNotEmpty()
  Username: string;

  @ApiModelProperty({example: 'sEcReTCode'})
  @IsNotEmpty()
  Code: string;
}
