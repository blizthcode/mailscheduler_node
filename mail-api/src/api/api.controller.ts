import { Body, Controller, Post} from '@nestjs/common';
import { ApiService } from './api.service';
import { MailDto } from './dto/mail.dto';
import {
  ApiCreatedResponse,
  ApiBadRequestResponse,
} from '@nestjs/swagger';

@Controller('api/v1')
export class ApiController {
  constructor(private readonly apiService: ApiService) {}

  @Post()
  @ApiCreatedResponse({ description: 'The mail was successfully passed for sending' })
  @ApiBadRequestResponse({ description: 'Validation error' })
  async addTerm(@Body() mailDto: MailDto) {
    return await this.apiService.sendMail(mailDto);
  }
}
