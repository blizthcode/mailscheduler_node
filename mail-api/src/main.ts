import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import {Transport} from '@nestjs/microservices';
import {ConfigService} from './config/config.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService: ConfigService = app.get(ConfigService);
  const options = new DocumentBuilder()
    .setTitle('Mail API')
    .setDescription('')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('v1/docs', app, document);

  app.useGlobalPipes(new ValidationPipe({transform: true}));

  await app.listen(3000);

  const micro = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.NATS,
    options: {
      url: configService.get('NATS_URL'),
    },
  });
  await micro.listenAsync();
}
bootstrap();
