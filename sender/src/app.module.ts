import { Module } from '@nestjs/common';
import { SendModule } from './send/send.module';
import {MailerModule} from '@nest-modules/mailer';
import { ConfigModule } from './config/config.module';

@Module({
  imports: [
    MailerModule.forRoot(),
    SendModule,
    ConfigModule],
})
export class AppModule {}
