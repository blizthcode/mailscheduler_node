import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {Transport} from '@nestjs/microservices';
import {ConfigService} from './config/config.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService: ConfigService = app.get(ConfigService);
  const micro = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.NATS,
    options: {
      url: configService.get('NATS_URL'),
    },
  });
  await micro.listenAsync();
}
bootstrap();
