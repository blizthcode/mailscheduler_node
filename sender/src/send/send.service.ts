import {Injectable} from '@nestjs/common';
import {MailDto} from '../dto/mail.dto';
import {MailerProvider} from '@nest-modules/mailer';

@Injectable()
export class SendService {
    constructor(private readonly mailerProvider: MailerProvider) {}

    async Send(data: MailDto) {
        await this.mailerProvider.sendMail(
            {
                to: data.To,
                from: data.From,
                subject: data.Subject,
                template: 'activation',
                context: {
                    username: data.Username,
                    code: data.Code,
                },
            },
        );
    }
}
