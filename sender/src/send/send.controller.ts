import {Controller} from '@nestjs/common';
import {
    MessagePattern,
} from '@nestjs/microservices';
import {MailDto} from '../dto/mail.dto';
import {Nodemailer} from 'nodemailer';
import {SendService} from './send.service';

@Controller('send')
export class SendController {
    constructor(private readonly sendService: SendService) {}

    @MessagePattern({ cmd: 'send'})
    async Send(data: MailDto) {
        await this.sendService.Send(data);
    }
}
