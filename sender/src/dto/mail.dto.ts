export class MailDto {
    From: string;

    To: string;

    Subject: string;

    Username: string;

    Code: string;
}
